# cards-opposing-people

Randomly generates [Cards Against Humanity](https://cardsagainsthumanity.com/) phrases and displays them on the Adafruit [PyPortal](https://www.adafruit.com/product/4116). NSFW.

Cards Against Humanity is available under a [Creative Commons BY-NC-SA 2.0 license](https://creativecommons.org/licenses/by-nc-sa/2.0/).

Included minified libraries from [adafruit-circuitpython-bundle-4.x-mpy-20191121.zip](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/download/20191121/adafruit-circuitpython-bundle-4.x-mpy-20191121.zip)

Tested with CircuitPython 4.1.

Installation:

- Copy files to your mounted CIRCUITPY device.
