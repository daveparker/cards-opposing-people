# The MIT License (MIT)

# Copyright (c) 2019 Dave Parker

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""
Randomly generates Cards Against Humanity phrases and displays them on
the Adafruit PyPortal. NSFW.
"""

import board
import displayio
import gc
import random
import time

from adafruit_display_text.label import Label
from adafruit_bitmap_font import bitmap_font

TOKEN = '{white}'

FONT_PATH = "/fonts/Arial-Bold-24.bdf"
BACKGROUND = "background.bmp"

MAX_LINES = 6
MAX_LINE_LENGTH = 17
MAX_CARD_LENGTH = 90
LINE_HEIGHT = 35

DELAY = 5

def generate_card(black_cards, white_cards):
    black = random.choice(black_cards)
    if black.find(TOKEN) == -1:
        return black + ' ' + random.choice(white_cards) + '.'
    while black.find(TOKEN) != -1:
        black = black.replace(TOKEN, random.choice(white_cards), 1)

    return black


def main():
    with open('black.txt') as black_file:
        black_cards = black_file.read().splitlines()

    with open('white.txt') as white_file:
        white_cards = white_file.read().splitlines()

    background_file = open(BACKGROUND, "rb")
    background_image = displayio.OnDiskBitmap(background_file)
    background_sprite = displayio.TileGrid(background_image, pixel_shader=displayio.ColorConverter())

    root_group = displayio.Group(max_size=2)
    root_group.append(background_sprite)

    font = bitmap_font.load_font(FONT_PATH)
    glyphs = b'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-+,. "\''
    font.load_glyphs(glyphs)

    loading = Label(font, text="Loading...")
    loading.x = 80
    loading.y = 120

    root_group.append(loading)
    board.DISPLAY.show(root_group)

    while True:
        try:
            card = generate_card(black_cards, white_cards)
            while len(card) > MAX_CARD_LENGTH:
                card = generate_card(black_cards, white_cards)

            words = card.split(' ')
            line = ""
            lines = []
            while True:
                if len(words) == 0:
                    lines.append(line)
                    break

                word = words.pop(0)
                if len(line) + len(word) < MAX_LINE_LENGTH:
                    line += word + ' '
                else:
                    lines.append(line)
                    line = word + ' '

            if len(lines) > MAX_LINES:
                continue

            label_group = displayio.Group(max_size=len(lines))
            for i, line in enumerate(lines):
                label = Label(font, text=line)
                label.x = 15
                label.y = (i + 1) * LINE_HEIGHT
                label_group.append(label)

            root_group.pop()
            root_group.append(label_group)
            time.sleep(DELAY)

            gc.collect()
        except:
            pass # so, so lazy


if __name__ == '__main__':
    main()
